# Escript

An escript example

## To build

    $ mix escripts.build

## To test

    $ mix test

## To run

    $ mix escript.build
    $ ./example --name=Marc
    > Hello Marc
