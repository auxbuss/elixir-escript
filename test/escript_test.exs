defmodule EscriptTest do
  use ExUnit.Case
  import ExUnit.CaptureIO

  test "happy path" do
    assert capture_io(fn ->
      Escript.main(["--name=fred"])
    end) == "Hello fred\n"
  end

  test "empty string, no args" do
    assert capture_io(fn ->
      Escript.main([""])
    end) == "No arguments given\n"
  end

  test "empty args" do
    assert capture_io(fn ->
      Escript.main([])
    end) == "No arguments given\n"
  end
end
